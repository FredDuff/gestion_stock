<?php
// #############################################################################
// INFO SERVEUR ################################################################
// #############################################################################

/*echo __DIR__ ."<br>";
echo $_SERVER['SERVER_NAME']."<br>";
echo $_SERVER['REMOTE_ADDR']."<br>";
die("Die ici ==> ".$_SERVER['PHP_SELF']); /* */

// Mode test - actif : 0-1
    define('MODE_TEST', 1);
// Liste des serveurs de dev    
    define('LISTE_SERVEUR_DEV', array('127.0.0.1','::1',));
// Liste des serveurs de prod    
    define('LISTE_SERVEUR_PROD', array('192.168.1.1','192.168.1.2',));


// #############################################################################
// LOCAL CONFIGURATION #########################################################
// #############################################################################
if ($_SERVER['SERVER_NAME']=="gestock" OR in_array($_SERVER['REMOTE_ADDR'], LISTE_SERVEUR_DEV)) {

    // Conf email support technique
    define('EMAIL_SUPPORT_TECH', 'duffau_frederic@hotmail.fr');
// Conf nom de domaine - Sert pour les communications
    define('WWW_NDD_GENERAL', 'gestock');
// Conf Database
    define('HOST_DATABASE', '127.0.0.1');
    define('USERNAME_DATABASE', 'root');
    define('PASSWORD_DATABASE', 'root');
    define('CHARSET_BDD', 'UTF8');
// Nom Bdd Applications
    define('DATABASE_NAME_GESTION_STOCK_DEV', 'gestion_stock'); // Bdd locale
    // define('DATABASE_NAME_GESTION_STOCK_ARCHIVE', 'gestion_stock_archive'); // Bdd archive
// Definition du path en local de la racine du projet
    define('PATH_MACHINE', 'C:/UwAmp/www/gestock/');
// Definition du path du host principal
    define('HTTP_PATH_HOST_PRINCIPAL', 'http://gestock/');
// Definition du path des views
    define('HTTP_PATH_VIEWS', 'views/');
// Var de sécurité pour l'authentification
    define('VAR_SECURE_AUTH', '!gR13nTr0uV3d3M13uX!');
// Langue par defaut
    define('LANGUE_PAR_DEFAUT', 'fra');
// Nom des cookies
    define('COOKIE_RESTER_CONNECTE', "qeliubii6512oups");
    define('COOKIE_HTTPS_ONLY', FALSE);
// Url api curl
    define('URL_CURL_API_REST', HTTP_PATH_HOST_PRINCIPAL.'api/rest/');

// #############################################################################
// EXT CONFIGURATION ###########################################################
// #############################################################################
} elseif ($_SERVER['SERVER_NAME']=="www.gestock.com" OR $_SERVER['REMOTE_ADDR']=="198.58.99.125") {

// ###############################################################
// ################ CONF DES SERVICES ############################
// ###############################################################

// ################ CONF PROD ###########################
// Conf email support technique
define('EMAIL_SUPPORT_TECH', 'contact_dev@gestock.com');
// Conf nom de domaine - Sert pour les communications
    define('WWW_NDD_GENERAL', 'www.gestock.com');
// Conf Database
    define('HOST_DATABASE', '127.0.0.1');
    define('USERNAME_DATABASE', 'root');
    define('PASSWORD_DATABASE', 'root');
    define('CHARSET_BDD', 'UTF8');
// Nom Bdd Applications
    define('DATABASE_NAME_GESTION_STOCK', 'gestion_stock'); // Bdd prod
    // define('DATABASE_NAME_GESTION_STOCK_ARCHIVE', 'gestion_stock_archive'); // Bdd archive
// Definition du path en local de la racine du projet
    define('PATH_MACHINE', 'C:/UwAmp/www/gestock/');
// Definition du path du host principal
    define('HTTP_PATH_HOST_PRINCIPAL', 'http://gestock/');
// Definition du path des views
    define('HTTP_PATH_VIEWS', 'views/');
// Var de sécurité pour l'authentification
    define('VAR_SECURE_AUTH', '!gR13nTr0uV3d3M13uX!');
// Langue par defaut
    define('LANGUE_PAR_DEFAUT', 'fra');
// Nom des cookies
    define('COOKIE_RESTER_CONNECTE', "qeliubii6512oups");
    define('COOKIE_HTTPS_ONLY', FALSE);
// Url api curl
    define('URL_CURL_API_REST', HTTP_PATH_HOST_PRINCIPAL.'api/rest/');
// #############################################################################
// SI ERREUR ###################################################################
// #############################################################################
} else {
    die("Une erreur c'est produite. ERROR A00000001.");
}
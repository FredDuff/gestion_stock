<?php

require_once('Models.php');

class PostModel extends Models {

    /**
     * Model for postCreateReader Controler
     *
     * @return object
     */
    public static function postAddUserModel() {
        $dbConnect = postModel::bddConnect();
        $insert = $dbConnect->prepare("INSERT INTO utilisateurs (prenom, nom, email, mot_de_passe, type, actif)
                                  VALUES (:prenom, :nom, :email, :mot_de_passe, :type, :actif)");
        return $insert;
    }

}

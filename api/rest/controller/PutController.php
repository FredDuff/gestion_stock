<?php
// general controller
require_once('Controllers.php');
// model
require_once('model/PutModel.php');

class PutController extends Controllers
{

    /**
     * Public method for access api.
     * This method dynmically call the method based on the query string
     * @param string $ctrl => function name
     * @return call function if exist
     */
    public function processApi($ctrl)
    {
        $ctrl = trim($ctrl);
        if ((int) method_exists($this, $ctrl) > 0) {
            $this->$ctrl($ctrl);
            // If the method not exist with in this class
        } else {
            if ($ctrl == "") {
                $ctrl = "no controller";
            }
            $this->responseResult(array('status' => 'failed', 'result' => 'Controller not found -> ' . $ctrl));
        }
    }

    /**
     * Désactive le produit correspondant à l'id
     *
     * @param string $ctrl => name controleur
     * @param array $param => param
     * @return json => text page
     */
    private function putDisableProduct($ctrl)
    {
        global $post_vars;
        // Call to model
        $update = putModel::putDisableProductModel();
        // Execute
        if (isset($post_vars['productId'])) {
            $param = array("id" => Controllers::secureForm($post_vars['productId']));
            if (preg_match('/\d+/', $param['id'])) {
                $this->tryCatchError($update, $param, $ctrl);
                // If not database error, success
                $resultFinal = array('ctrl' => $ctrl, 'status' => 'success', 'result' => "Produit désactivé");
                $this->responseResult($resultFinal);
            }
        }
        $resultFinal = array('ctrl' => $ctrl, 'status' => 'failed', 'result' => "Paramètre incorrect");
        $this->responseResult($resultFinal);
    }

    /**
     * Désactive l'utilisateur correspondant à l'id
     *
     * @param string $ctrl => name controleur
     * @param array $param => param
     * @return json => text page
     */
    private function putDisableUser($ctrl)
    {
        global $post_vars;
        // Call to model
        $update = putModel::putDisableUserModel();
        // Execute
        if (isset($post_vars['userId'])) {
            $param = array("id" => Controllers::secureForm($post_vars['userId']));
            if (preg_match('/\d+/', $param['id'])) {
                $this->tryCatchError($update, $param, $ctrl);
                // If not database error, success
                $resultFinal = array('ctrl' => $ctrl, 'status' => 'success', 'result' => "Utilisateur désactivé");
                $this->responseResult($resultFinal);
            }
        }
        $resultFinal = array('ctrl' => $ctrl, 'status' => 'failed', 'result' => "Paramètre incorrect");
        $this->responseResult($resultFinal);
    }
}

// Initiiate Library
if ($keySecure == "cle3") {
    $api = new PutController;
    $api->processApi(Controllers::secureForm($post_vars['ctrl']));
} else {
    header('Location: unauthorizedAccess');
    die();
}

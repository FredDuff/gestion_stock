<?php
/**
 * General controllers
 * Get = select
 * Post = insert
 * Put = update
 * Delete = delete
 */
class Controllers {

// ####################################################################### PARTIE SECURISATION
/**
   * Sécurise les variables d'un tableau clé/valeur
   *
   * @param array => Tableau
   * @return array => Tableau avec les valeur après sécurisation : supprime les balises HTML et PHP d'une chaine de caractère,
   * remplace les espaces trops longs,
   * traduit les entités HTML dans une chaine de caractères,
   * enleve les espaces du début et de fin,
   */
  public static function secureArray($array)
  {
      $newArray = array();
      foreach ($array as $key => $value) {
          // Si la valeur n'est pas un tableau
          if (!is_array($value)) {
              // Sinon on remet les valeurs telles quelles
              $newArray[$key] = Controllers::secure($value); // Remplace dans le tableau l'ancienne valeur par la nouvelle
            } else {
                $newArray[$key] = Controllers::secureArray($value);
            }
        }
      return $newArray;
  }

  private static function secure($value)
  {
      $value = strip_tags($value); // Supprime les balises HTML et PHP d'une chaine de caractère
      $value = htmlspecialchars($value); // Traduit les entités HTML dans une chaine de caractères
      $value = Trim($value); // Enleve les espaces du début et de fin
      return $value;
  }

// #######################################################################

    /**
     * Vérif état connexion
     * 
     * @return boolean
     */
    public static function isConnected() {
        return (isset($_SESSION['idUser']) && !empty($_SESSION['idUser'])) ? true : false;
    }

    /**
     * Vérif connexion
     * 
     * @return object
     */
    static function verifUserIfExist($email, $pwd) {
        $info = new \stdClass();
        $resultGetCurl = Controllers::getUsers();
        if($resultGetCurl) {
            if ($resultGetCurl->status=="failed") {
                $info->status = 'danger';
                $info->message = "Une erreur est survenue ! Veuillez contacter le support technique!";
            } elseif ($resultGetCurl->status=="success") {
                $info->status = 'danger';
                $info->message = "L'adresse email et le mot de passe que vous avez entrés ne correspondent pas à ceux présents dans nos fichiers. Veuillez vérifier et réessayer.";
                foreach ($resultGetCurl->result as $user) {
                    if ($email == $user->email && hash('sha512', $pwd) == $user->mot_de_passe) {
                        $_SESSION['idUser'] = $user->id;
                        $_SESSION["nameUser"] = $user->prenom;
                        $_SESSION["lastNameUser"] = $user->nom;
                        $_SESSION["emailUser"] = $user->email;
                        $_SESSION["typeUser"] = $user->type;
                        $info->status = 'success';
                        $info->message = "Heureux de vous revoir $user->prenom";
                        break;
                    }
                }
            }
        } else {
            $info->status = 'danger';
            $info->message = "Un problème sur nos serveurs ne nous permet pas de vous identifier actuellement, merci de contacter le support technique.";
        }
        return $info;
    }

    /**
     * Déconnexion
     */
    static function signOut() {
        unset($_SESSION["idUser"]);
        unset($_SESSION["nameUser"]);
        unset($_SESSION["lastNameUser"]);
        unset($_SESSION["emailUser"]);
        unset($_SESSION["typeUser"]);
    }

    static function getUsers()
    {
        return json_decode(Controllers::getCurlRest("?ctrl=getUsers"));
    }

    static function getProducts()
    {
        return json_decode(Controllers::getCurlRest("?ctrl=getProducts"));
    }

     /**
     * Sert a traiter l'ajout des users
     * @param array $datas => les datas
     * @return object => result
     */
    static function addUser($datas) {
        $info = new \stdClass();
        $param = array(
                "typeSend" =>"POST",
                "ctrl" => "postAddUser",
                "datas" => $datas,
                );
        $postResult = json_decode(Controllers::postCurlRest($param));
        if ($postResult->status == 'success') {
            $info->status = 'success';
            $info->message = "Utilisateur créé avec succès";
        } else {
            $info->status = 'danger';
            $info->message = "L'opération a échoué";
        }
        // var_dump($datas);
        // var_dump($param);
        // var_dump($postResult);
        // die();
        return $info;

    }

    /**
     * Désactive un utilisateur
     *
     * @param int $userid
     * @return object => result
     */

    static function disableUser($userId) {
        $info = new \stdClass();
        $param = array(
            "typeSend" => "PUT",
            "ctrl" => "putDisableUser",
            "userId" => $userId,
        );
        $putResult = json_decode(Controllers::putCurlRest($param));
        if ($putResult->status == 'success') {
            $info->status = 'success';
            $info->message = "Utilisateur désactivé";
        } else {
            $info->status = 'danger';
            $info->message = "L'opération a échoué";
        }
        // var_dump($param);
        // var_dump($putResult);
        // die();
        return $info;
    }

    static function disableProduct($productId) {
        $info = new \stdClass();
        $param = array(
            "typeSend" => "PUT",
            "ctrl" => "putDisableProduct",
            "productId" => $productId,
        );
        $putResult = json_decode(Controllers::putCurlRest($param));
        if ($putResult->status == 'success') {
            $info->status = 'success';
            $info->message = "Produit désactivé";
        } else {
            $info->status = 'danger';
            $info->message = "L'opération a échoué";
        }
        // var_dump($param);
        // var_dump($putResult);
        // die();
        return $info;
    }

    /**
     * Ecrit "Bonjour" avec le prénom récupéré ou "Bonjour à vous" si pas de prénom récupéré
     * 
     * @param string=> Prénom récupéré
     * @return string=> renvoi un texte avec la var récupérée
     */
    public static function writingHello($maVar) {
        if (isset($maVar) && $maVar != "") {
            $returnText = "Bonjour " . $maVar;
        } else {
            $returnText = "Bonjour à vous !!!";
        };
    return $returnText;
    }

    /**
     * Call curl on API in REST
     * @param int $param => params for call api
     * @return json => result API
     */
    static function getCurlRest($param) {
        // Curl init
        // var_dump($param);
        // die('getcurlrest');
        $curl = curl_init();
        // Curl config
        curl_setopt($curl, CURLOPT_URL, URL_CURL_API_REST . $param);
        curl_setopt($curl, CURLOPT_FAILONERROR, true);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 3);
        curl_setopt($curl, CURLOPT_TIMEOUT, 20);
        // Curl result
        $result = curl_exec($curl);
        /* echo "<pre>";
        print_r($result);
        echo "</pre>"; /* */
        curl_close($curl);
        return $result;
    }

    /**
     * Call curl on API in REST
     * @param array $param => params for call api
     * @return json => result API
     */
    static function postCurlRest($param) {
        // Treatment param
        $postVars = http_build_query($param);
        // Curl init
        $curl = curl_init();
        // Curl config
        curl_setopt($curl, CURLOPT_URL, URL_CURL_API_REST);
        curl_setopt($curl, CURLOPT_POST, 1);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $postVars);
        curl_setopt($curl, CURLOPT_FAILONERROR, true);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 3);
        curl_setopt($curl, CURLOPT_TIMEOUT, 20);
        // Curl result
        $result = curl_exec($curl);
        /* echo "<pre>";
          print_r($result); /* */
        curl_close($curl);
        return $result;
    }

    /**
     * Call curl on API in REST on PUT method
     * @param array $param => params for call api
     * @return json => result API
     */
    static function putCurlRest($param) {
        // Treatment param
        $postVars = http_build_query($param);
        // Curl init
        $curl = curl_init();
        // Curl config
        curl_setopt($curl, CURLOPT_URL, URL_CURL_API_REST);
        curl_setopt($curl, CURLOPT_POST, 1);
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "PUT");
        curl_setopt($curl, CURLOPT_POSTFIELDS, $postVars);
        curl_setopt($curl, CURLOPT_FAILONERROR, true);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 3);
        curl_setopt($curl, CURLOPT_TIMEOUT, 20);
        // Curl result
        $result = curl_exec($curl);
        // For test
          /* echo "<pre> curlComponent => putCurlRest<hr>";
          print_r($result);
          echo "</pre>"; /* */
        curl_close($curl);
        return $result;
    }

    /**
     * Call curl on API in REST on DELETE method
     * @param array $param => params for call api
     * @return json => result API
     */
    static function deleteCurlRest($param) {
        // Treatment param
        $postVars = http_build_query($param);
        // Curl init
        $curl = curl_init();
        // Curl config
        curl_setopt($curl, CURLOPT_URL, URL_CURL_API_REST);
        curl_setopt($curl, CURLOPT_POST, 1);
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "DELETE");
        curl_setopt($curl, CURLOPT_POSTFIELDS, $postVars);
        curl_setopt($curl, CURLOPT_FAILONERROR, true);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 3);
        curl_setopt($curl, CURLOPT_TIMEOUT, 20);
        // Curl result
        $result = curl_exec($curl);
        // var_dump($result);
        curl_close($curl);
        return $result;
    }
}

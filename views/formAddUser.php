<div class="sideContent mx-auto w-50 text-center">
  <form class="p-3" action="./index.php" method="post">
    <input type="hidden" name="action" value="addUser">
    <input type="hidden" name="page" value="listUsers">
    <h1 class="h3 mb-3 font-weight-normal">Créer un compte ...</h1>
    <label for="inputName" class="sr-only">Prénom</label>
    <input type="text" id="inputName" name="name" class="form-control form-control-first" value="" placeholder="Prénom" required autofocus>
    <label for="inputName" class="sr-only">Nom</label>
    <input type="text" id="inputLastName" name="lastname" class="form-control" value="" placeholder="Nom" required>
    <label for="inputEmail" class="sr-only">Adresse email</label>
    <input type="email" id="inputEmail" name="email" class="form-control" value="" placeholder="Adresse email" required>
    <label for="inputPassword" class="sr-only">Mot de passe</label>
    <!-- <input type="password" id="inputPassword" name="password" class="form-control" value="" placeholder="Mot de passe" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}" title="Doit contenir au moins un chiffre, une lettre majuscule, une lettre minuscule et au moins 8 charactères" > -->
    <input type="password" id="inputPassword" name="password" class="form-control" value="" placeholder="Mot de passe" required>
    <label for="selectType" class="sr-only">Type</label>
    <select id="selectType" name="type" class="form-control form-control-last">
      <option>admin</option>
      <option>guest</option>
      <option selected>client</option>
    </select>
    <div class="btn-group" role="group" aria-label="Validation button group">
      <button class="btn btn-lg btn-primary" type="submit">Valider</button>
      <button class="btn btn-lg btn-danger" type="reset">Annuler</button>
    </div>
  </form>
</div>
        <footer class="footer mt-auto py-1 bg-dark">
            <nav class="navbar navbar-expand navbar-dark bg-dark justify-content-center">
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <a class="nav-link" href="https://www.economie.gouv.fr/entreprises/reglement-general-sur-protection-des-donnees-rgpd#"><small>RGPD</small></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#"><small>Conditions générales</small></a>
                    </li>
                    <li class="nav-item">
                        <address class="mb-0"><a class="nav-link" href="mailto:contact_dev@gestock.com"><small>Le developpeur à votre service</small></a></address>
                    </li>
                </ul>
            </nav>    
        </footer>
    </body>
</html>
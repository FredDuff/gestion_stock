<div class="sideContent mx-auto text-center">
  <form class="form-signin" action="./index.php" method="post">
    <input type="hidden" name="action" value="auth">
    <img src="/component/img/logoGeStock.png" alt="Logo GeStock">
    <h1 class="h3 mb-3 font-weight-normal">Connexion ...</h1>
    <label for="inputEmail" class="sr-only">Adresse email</label>
    <input type="email" id="inputEmail" name="email" class="form-control form-control-first" placeholder="Adresse email" required autofocus>
    <label for="inputPassword" class="sr-only">Mot de passe</label>
    <!-- <input type="password" id="inputPassword" name="password" class="form-control" placeholder="Mot de passe" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}" title="Doit contenir au moins un chiffre, une lettre majuscule, une lettre minuscule et au moins 8 charactères" > -->
    <input type="password" id="inputPassword" name="password" class="form-control form-control-last" placeholder="Mot de passe">
    <button class="btn btn-lg btn-primary btn-block mb-2" type="submit">Connexion</button>
    <a href="">Mot de passe oublié ?</a><span class="mx-2">·</span><a href="">Créer un compte</a>
    <p class="mt-2 text-muted">&copy; 2020</p>
  </form>
</div>
<div id="mainContainer" class="container-fluid position-relative ">
    <?php
    if ($info->status) {
        echo '
            <div id="info" class="row justify-content-center">
                <div class="alert alert-'.$info->status.' alert-dismissible fade show position-absolute" role="alert">
                    <span class="small">'.$info->message.'</span>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            </div>
        ';
    }
?>
    <div class="row flex-wrap flex-md-nowrap">
        <?php
            if (Controllers::isConnected()) {
                require_once ("sideMenu.php");
                if (isset($arrayVar['page'])){
                    $pageCalled=$arrayVar['page'];
                    switch ($pageCalled){
                        case 'addUsers':
                            require_once("formAddUser.php");
                            break;
                        case 'listUsers':
                            require_once("usersTableView.php");
                            break;
                        case 'statistics':
                            require_once("statistiques.php");
                            break;
                        case 'listProducts':
                            require_once("ProductsTableView.php");
                            break;
                        default:
                            require_once("default.php");
                            break;
                    }
                } else {
                    require_once("default.php");
                }
            } else {
                require_once ("formConnection.php");        
            }
        ?>
    </div>
</div>
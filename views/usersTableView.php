<?php
    $resultGetUsers = Controllers::getUsers();
?>
        <div class="sideContent flex-grow-1 d-flex flex-column overflow-hidden">
            <h1 class="mt-2 mx-auto">Liste des Clients</h1>
            <!-- TODO TableView -->
            <div class="m-2 p-2 bg-light overflow-hidden">
                <table class="table table-striped table-bordered" id="usersDataTable">
                    <thead>
                        <tr>
                        <?php
                            foreach ($resultGetUsers->result[0] as $key => $val) {
                        ?>
                            <th><?php echo ucfirst($key) ?></th>
                        <?php
                            }
                        ?>
                            <th>Editer / Supprimer</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php
                    foreach ($resultGetUsers->result as $user) {
                    ?>
                        <tr>
                        <?php
                        foreach ($user as $val) {
                        ?>
                            <td><?php echo $val ?></td>
                        <?php
                        }
                        ?>
                            <td>
                                <div class="d-flex justify-content-around align-items-center">
                                    <a href="index.php?page=listUsers&action=editUser&id=<?php echo $user->id ?>" ><i class="fas fa-edit text-dark"></i></a>
                                    <form method="post" action="index.php?page=listUsers">
                                        <input type="hidden" name="action" value="disableUser">
                                        <input type="hidden" name="id" value="<?php echo $user->id ?>">
                                        <button type="submit" class="btn btn-link"><i class="far fa-trash-alt text-dark"></i></button>
                                    </form>
                                </div>
                            </td>
                        </tr>
                    <?php
                    }
                    ?>
                    </tbody>
                </table>
            </div>
        </div> 
        <script>
            $('#usersDataTable').DataTable();
        </script>
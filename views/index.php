<?php
// Lancement de la session
session_start();
// Encodage de la page
header ("Content-Type: text/html; charset=utf-8");
// Conf générale
require_once ("../conf/generalConf.php");
// Autoload global
require_once (PATH_MACHINE."autoLoader/AutoLoad.php");

// Pour afficher les erreurs php
    if (MODE_TEST==1) {
        //echo "Test activé : ".HOST_DATABASE."<br>";
        error_reporting(E_ALL);
        ini_set("display_errors", 1);
    }

// Sécurisation des vars reçus
    $arrayVar = Controllers::secureArray($_REQUEST);
// var_dump($arrayVar);

$info = new \stdClass();
$info->status = false;
// Detection si le formulaire a été lancer (if action=="auth")

if (isset($arrayVar["action"]) && $arrayVar["action"] !== "") {
    $action = $arrayVar["action"];
    if (Controllers::isConnected()) {
        switch ($action) {
            case 'signOut':
                Controllers::signOut();
                break;
            case 'addUser':
                if (   isset($arrayVar['name'])     && !empty($arrayVar['name']) 
                    && isset($arrayVar['lastname']) && !empty($arrayVar['lastname'])
                    && isset($arrayVar['email'])    && filter_var($arrayVar["email"], FILTER_VALIDATE_EMAIL)
                    // && isset($arrayVar['password']) && preg_match('/(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}/', $arrayVar["password"])
                    && isset($arrayVar['password']) && !empty($arrayVar['password'])
                    && isset($arrayVar['type'])     && !empty($arrayVar['type'])
                ) {
                    $newUser = array(
                        'name' => $arrayVar['name'],
                        'lastname' => $arrayVar['lastname'],
                        'email' => $arrayVar['email'],
                        'password' => $arrayVar['password'],
                        'type' => $arrayVar['type'],
                        'active' => 1);
                    $info = Controllers::addUser($newUser);
                } else {
                    $info->status = 'danger';
                    $info->message = "L'opération a échoué";
                }
                break;
            case 'disableUser':
                if (isset($arrayVar['id']) && preg_match('/\d+/', $arrayVar['id'])) {
                    $info = Controllers::disableUser($arrayVar['id']);
                }
                break;
            case 'disableProduct':
                if (isset($arrayVar['id']) && preg_match('/\d+/', $arrayVar['id'])) {
                    $info = Controllers::disableProduct($arrayVar['id']);
                }
                break;
            default:
                break;
        }
    } elseif ($arrayVar["action"]=="auth") {
        if (isset($arrayVar["email"]) && isset($arrayVar["password"])) {
            // if (filter_var($arrayVar["email"], FILTER_VALIDATE_EMAIL) && preg_match('/(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}/', $arrayVar["password"])) {
            if (filter_var($arrayVar["email"], FILTER_VALIDATE_EMAIL) && !empty($arrayVar["password"])) {
                $info = Controllers::verifUserIfExist($arrayVar["email"], $arrayVar["password"]);
            } else {
                $info->status = 'danger';
                $info->message = 'Merci de renseigner correctement les champs';
            }
        }
    }
}

// Appel header général
    require_once ("header.php");

// Appel body général
   require_once ("main.php");

// Appel footer général
    require_once ("footer.php");

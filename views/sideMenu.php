        <nav class="navbar navbar-expand-md col-12 col-md-auto flex-column bg-light p-0">
            <div id="sideMenu" class="collapse navbar-collapse flex-column justify-content-start py-2">
                <div class="w-100 px-1 border-bottom">
                    <img src="/component/img/avatar/gastIcon.jpg" class="img-thumbnail rounded mr-1 float-left" height="60" width="60" alt="Avatar de l'utilisateur">
                <?php
                if (Controllers::isConnected()){
                ?>
                    <p><b><?php echo $_SESSION['nameUser'] ?></b></p>
                    <a class="mr-2 text-secondary" href="#">Profil</a><a class="text-secondary" href="./index.php?action=signOut"><i class="fas fa-lock"></i> Déconnexion</a>
                <?php
                } else {
                ?>
                    <a class="text-secondary"><i class="fas fa-lock"></i> Connexion</a>
                <?php
                }
                ?>
                </div>
                <?php
                    if (Controllers::isConnected()){
                ?>
                <div class="d-flex flex-column">
                    <div class="text-center">
                        <h5 class="mt-2"><i class="fas fa-desktop"></i> Tableau de bord</h5>
                    </div>
                    <div class="d-flex justify-content-around px-3">
                        <button onclick="window.location.href = 'index.php?page=TODO';" class="btn btn-green btn-square rounded-0 m-1 p-0"><i class="fas fa-shopping-cart"></i><br>Entrée</button>
                        <button onclick="window.location.href = 'index.php?page=TODO';" class="btn btn-orange btn-square rounded-0 m-1 p-0"><i class="fas fa-shopping-cart"></i><br>Sortie</button>
                    </div>
                    <div class="d-flex justify-content-around px-3">
                        <button onclick="window.location.href = 'index.php?page=addUsers';" class="btn btn-purple btn-square rounded-0 m-1 p-0"><i class="fas fa-user"></i><br>Utilisateurs</button>
                        <button onclick="window.location.href = 'index.php?page=statistics';" class="btn btn-pink btn-square rounded-0 m-1 p-0"><i class="far fa-chart-bar"></i><br>Statistiques</button>
                    </div>
                    <div class="d-flex justify-content-around px-3">
                        <button onclick="window.location.href = 'index.php?page=listProducts';" class="btn btn-orange btn-square rounded-0 m-1 p-0"><i class="fas fa-table"></i><br>Stock</button>
                        <button onclick="window.location.href = 'index.php?page=TODO';" class="btn btn-teal btn-square rounded-0 m-1 p-0"><i class="far fa-clock"></i><br>Journal</button>
                    </div>
                    <div class="d-flex justify-content-around px-3">
                        <button onclick="window.location.href = 'index.php?page=listUsers';" class="btn btn-red btn-square rounded-0 m-1 p-0"><i class="fas fa-user"></i><br>Client</button>
                        <button onclick="window.location.href = 'index.php?page=TODO';" class="btn btn-cyan btn-square rounded-0 m-1 p-0"><i class="fas fa-sitemap"></i><br>Catégorie</button>
                    </div>
                </div>
                        
                <?php
                    }
                ?>
            </div>
        </nav>
<?php
    $resultGetProducts = Controllers::getProducts();
?>
        <div class="sideContent flex-grow-1 d-flex flex-column overflow-hidden">
            <h2 class="mt-2 mx-auto">Liste des Produits</h2>
            <!-- TODO TableView -->
            <div class="m-2 p-2 bg-light overflow-hidden">
                <table class="table table-striped table-bordered" id="productsDataTable">
                    <thead>
                        <tr>
                        <?php
                            foreach ($resultGetProducts->result[0] as $key => $val) {
                        ?>
                            <th><?php echo ucfirst($key) ?></th>
                        <?php
                            }
                        ?>
                            <th>Editer / Supprimer</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php
                    foreach ($resultGetProducts->result as $product) {
                    ?>
                        <tr>
                        <?php
                        foreach ($product as $val) {
                        ?>
                            <td><?php echo $val ?></td>
                        <?php
                        }
                        ?>
                            <td>
                                <div class="d-flex justify-content-around align-items-center">
                                    <a href="" ><i class="fas fa-edit text-dark"></i></a>
                                    <form method="post" action="index.php?page=listProducts">
                                        <input type="hidden" name="action" value="disableProduct">
                                        <input type="hidden" name="id" value="<?php echo $product->id ?>">
                                        <button type="submit" class="btn btn-link"><i class="far fa-trash-alt text-dark"></i></button>
                                    </form>
                                </div>
                            </td>
                        </tr>
                    <?php
                    }
                    ?>
                    </tbody>
                </table>
            </div>
        </div> 
        <script>
            $('#productsDataTable').DataTable();
        </script>
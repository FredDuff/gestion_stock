<?php

$page_redirected_from = $_SERVER['REQUEST_URI'];  // this is especially useful with error 404 to indicate the missing page.
$server_url = "http://" . $_SERVER["SERVER_NAME"] . "/";
$redirect_url = $_SERVER["REDIRECT_URL"];
$redirect_url_array = parse_url($redirect_url);
$end_of_path = strrchr($redirect_url_array["path"], "/");

switch (getenv("REDIRECT_STATUS")) {
        # "400 - Bad Request"
    case 400:
        $error_code = "400 - Bad Request";
        $explanation = "The syntax of the URL submitted by your browser could not be understood. Please verify the address and try again.";
        $redirect_to = "";
        break;

        # "401 - Unauthorized"
    case 401:
        $error_code = "401 - Unauthorized";
        $explanation = "This section requires a password or is otherwise protected. If you feel you have reached this page in error, please return to the login page and try again, or contact the webmaster if you continue to have problems.";
        $redirect_to = "";
        break;

        # "403 - Forbidden"
    case 403:
        $error_code = "403 - Forbidden";
        $explanation = "This section requires a password or is otherwise protected. If you feel you have reached this page in error, please return to the login page and try again, or contact the webmaster if you continue to have problems.";
        $redirect_to = "";
        break;

        # "404 - Not Found"
    case 404:
        $error_code = "404 - Not Found";
        $explanation = "The requested resource '" . $page_redirected_from . "' could not be found on this server. Please verify the address and try again.";
        //$redirect_to = $server_url . "wiki" . $end_of_path;
        $redirect_to = "";
        break;
}
?>
<!DOCTYPE html>
<html lang="fr">
<head>
<meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css"/>
    <link rel="stylesheet" href="/component/css/style.css">

    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js"></script>
    <script src="https://kit.fontawesome.com/ae0054abd7.js" crossorigin="anonymous"></script>
    <script src="/component/js/main.js"></script>
    
    <link rel="icon" type="image/png" href="/component/img/favicon/favicon.png" />
    
    <?php
    if ($redirect_to != "") {
    ?>

        <meta http-equiv="Refresh" content="5; url='<?php print($redirect_to); ?>'">
    <?php
    }
    ?>

    <title>Page not found: <?php print($redirect_to); ?></title>

</head>

<body>

    <h1>Error Code <?php print($error_code); ?></h1>

    <p>The <a href="http://en.wikipedia.org/wiki/Uniform_resource_locator">URL</a> you requested was not found. <?PHP echo ($explanation); ?></p>

    <p><strong>Did you mean to type <a href="<?php print($redirect_to); ?>"><?php print($redirect_to); ?></a>?</strong> You will be automatically redirected there in five seconds.</p>

    <p>You may also want to try starting from the home page: <a href="<?php print($server_url); ?>"><?php print($server_url); ?></a></p>

    <hr />

    <p><i>A project of <a href="<?php print($server_url); ?>"><?php print($server_url); ?></a>.</i></p>

</body>

</html>